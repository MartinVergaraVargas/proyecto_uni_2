
public class Odometro {
	
	private float velocidad_actual, aceleracion;
	private float km_actual;
	
	public Odometro() {
		this.velocidad_actual = 0;
		
		this.km_actual = 0;
	}
	
	public float variacion_efectiva(float variacion_velocidad, float indice_combustible, float aerodinamica_actual) {
		double ln = (Math.log(aerodinamica_actual));
		double resistencia_viento = (100f / ln), variable_intermedia = (variacion_velocidad * indice_combustible), variacion_efectiva = (variable_intermedia - resistencia_viento);
		float variacion_efectivaf = Math.round(variacion_efectiva);
		return variacion_efectivaf;
	}
	
	
	public float acelerador(float variacion_efectiva, float velocidad_actual) {
		
		float velocidad_nueva = (velocidad_actual + variacion_efectiva);

		return velocidad_nueva;
	}
	
	
	public float actualizador_km_acelerando(float km_actual, float velocidad_actual, float aceleracion) {
		float transformador_de_velocidades = (1000f / 3600f), v_a = (velocidad_actual * transformador_de_velocidades);
		float ac = (aceleracion * transformador_de_velocidades);
		float velocidad_media = ((v_a + ac)/2f), distancia_recorrida = (velocidad_media * 15f), distancia_recorrida_en_km = (distancia_recorrida / 1000f);
		
		float km_nuevo = km_actual + (distancia_recorrida_en_km);
		return km_nuevo;
	}
	
	public float actualizador_km_a_velocidad_constante(float km_actual,float velocidad_actual) {		
		float transformador_de_velocidades = (1000f / 3600f), v_a = (velocidad_actual * transformador_de_velocidades);
		float distancia_recorrida = (v_a * 15f), distancia_recorrida_en_km = (distancia_recorrida / 1000f);
		float km_nuevo = (km_actual + distancia_recorrida_en_km);
		return km_nuevo;
	}
	

	

	
	public float getVelocidad_actual() {
		return velocidad_actual;
	}
	public void setVelocidad_actual(float velocidad) {
		this.velocidad_actual = velocidad;
	}

	



	public float getAceleracion() {
		return aceleracion;
	}


	public void setAceleracion(float aceleracion) {
		this.aceleracion = aceleracion;
	}


	public float getKm_actual() {
		return km_actual;
	}


	public void setKm_actual(float km_actual) {
		this.km_actual = km_actual;
	}
	
	
	
	
}
