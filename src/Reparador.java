
public class Reparador {
	
	private int ubicacion;
	private int velocidad;
	
	Reparador(){}

	public int getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(int ubicacion) {
		this.ubicacion = ubicacion;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
		
	public float reparacion_alas(float resistencia_actual) {
		float resistencia_faltante = (2000 - resistencia_actual);
		float curacion = (resistencia_faltante / 3);
		float resistencia_nueva = (resistencia_actual + curacion); 
		return resistencia_nueva;
	}
	
	public float reparacion_motor(float resistencia_actual, float resistencia_total) {
		float resistencia_faltante = (resistencia_total - resistencia_actual);
		float curacion = (resistencia_faltante / 3);
		float resistencia_nueva = (resistencia_actual + curacion);
		
		
		return resistencia_nueva;
	}

}
