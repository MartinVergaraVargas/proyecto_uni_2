import java.util.Collections;
import java.util.Scanner;
import java.util.Comparator;
public class Juego {
	
	private Pista pista;
	
	Scanner sc = new Scanner(System.in);

	
	boolean playing = true;

	Juego(){
		
		int cantidad = 4;
		
		// Cada segundo del código es equivalente a 10 minutos de carrera //
		//int diezminutos = 1000;
		
		this.pista = new Pista(cantidad);
		new Locutor();
		int tiempo_en_carrera = 0, finalistas = 0;
		
		System.out.println("Bienvenido a la race ");
		//System.out.println("Ingrese su nombre");
		
		
		
		System.out.println("El largo de la pista es 6000 km");
		System.out.println("A velocidad media (3000 km/h) la carrera duraria 2 horas");
		
		//aqui ya tenemos las naves puestas y las dimensiones de pista listas.
		//solo nos queda iniciar la carrera
		
		
		
		while ( playing ) {
			
			
			try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                
            }
			
			
			for (Nave n: pista.getNaves()) {
				
				float parametro_de_control_de_resistencia = (n.getMotor().getResistenciaM() / 4);
				
				if (n.getOdometro().getVelocidad_actual() == 0 && n.getAndando() == true && n.getEstado() != "a por reparacion" || n.getDelanteros() >= 2 && n.getAndando() == true && n.getEstado() != "a por reparacion") {
					// Estado que se gatillará cuando empiecne la carrera y cuando se terminen de reparar
					n.setEstado("acelerando");
				}
				
				else if (n.getDelanteros() <= 1 == n.getAndando() == true) {
					// Estado que se gatillará cuando evalúen que la mejor opción es avanzar a velocidad constante
					n.setEstado("vel constante");
				}
				
				
				else if (n.getDerecha().getResistencia_actual() <= 400 && n.getAndando() == true || n.getMotor().getResistencia_actual() <= parametro_de_control_de_resistencia && n.getAndando() == true) {
					// Estado que se gatillará cuando la nave esté por necesitar reparación
					n.setEstado("a por reparacion");
				}
				
				else if (n.getDerecha().getResistencia_actual() == 0 && n.getAndando() == false || n.getMotor().getResistencia_actual() == 0 && n.getAndando() == false || n.getEstanque().getCantidad_actual() == 0 && n.getAndando() == false) {
					// Estado que se gatillará cuando la nave necesite reparación
					n.setEstado("reparar");
				}
				
			}
			
			
			for (Nave n: pista.getNaves()) {
				
				if (n.getEstado() == "acelerando") {
					float aceleracion = n.getMotor().capacidad_de_aceleracion_en_funcion_de_la_velocidad_actual(n.getMotor().getCapacidad_de_aceleracion(), n.getOdometro().getVelocidad_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor());
					float variacion_efectiva = n.getOdometro().variacion_efectiva(aceleracion, n.getEstanque().getCombustible().getIndice_aceleracion(), n.getDerecha().getAerodinamica_actual());
					n.getMotor().setResistencia_actual(n.getMotor().actualizador_resistencia_acelerando(n.getOdometro().getVelocidad_actual(), variacion_efectiva, n.getMotor().getResistencia_actual(), n.getMotor().getResistenciaM(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getDerecha().setResistencia_actual(n.getDerecha().actualizador_resistencia_acelerando(n.getDerecha().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), variacion_efectiva, n.getDerecha().getResistencia_actual(), n.getDerecha().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getDerecha().setAerodinamica_actual(n.getDerecha().actualizador_aerodinamica_acelerando(n.getDerecha().getAerodinamica_actual(), n.getDerecha().getAerodinamica(), n.getOdometro().getVelocidad_actual(), variacion_efectiva, n.getDerecha().getResistenciaA(), n.getDerecha().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getIzquierda().setResistencia_actual(n.getIzquierda().actualizador_resistencia_acelerando(n.getIzquierda().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), variacion_efectiva, n.getIzquierda().getResistencia_actual(), n.getIzquierda().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getIzquierda().setAerodinamica_actual(n.getIzquierda().actualizador_aerodinamica_acelerando(n.getIzquierda().getAerodinamica_actual(), n.getIzquierda().getAerodinamica(), n.getOdometro().getVelocidad_actual(), variacion_efectiva, n.getIzquierda().getResistenciaA(), n.getIzquierda().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getEstanque().setCantidad_actual(n.getEstanque().actualizador_combustible_acelerando(n.getEstanque().getCombustible().getQuemaje(), n.getEstanque().getCantidad_actual(), n.getOdometro().getVelocidad_actual(), variacion_efectiva));
					
					n.getOdometro().setKm_actual(n.getOdometro().actualizador_km_acelerando(n.getOdometro().getKm_actual(), n.getOdometro().getVelocidad_actual(), variacion_efectiva));
					n.getOdometro().setVelocidad_actual(n.getOdometro().acelerador(variacion_efectiva, n.getOdometro().getVelocidad_actual()));	
					
										
					if (n.getMotor().getResistencia_actual() <= 0f) {
						n.getMotor().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getResistencia_actual() <= 0f) {
						n.getDerecha().setResistencia_actual(0f);
						n.getIzquierda().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getAerodinamica_actual() <= 0f) {
						n.getDerecha().setAerodinamica_actual(0f);
						n.getDerecha().setAerodinamica_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getEstanque().getCantidad_actual() <= 0f) {
						n.getEstanque().setCantidad_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
				}
				
				
				if (n.getEstado() == "vel constante") {					
					n.getMotor().setResistencia_actual(n.getMotor().actualizador_resistencia_a_velocidad_constante(n.getOdometro().getVelocidad_actual(), n.getMotor().getResistencia_actual(), n.getMotor().getResistenciaM(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getDerecha().setResistencia_actual(n.getDerecha().actualizador_resistencia_a_velocidad_constante(n.getDerecha().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), n.getDerecha().getResistencia_actual(), n.getDerecha().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getDerecha().setAerodinamica_actual(n.getDerecha().actualizador_aerodinamica_a_vel_constante(n.getDerecha().getAerodinamica_actual(), n.getDerecha().getAerodinamica(), n.getOdometro().getVelocidad_actual(), n.getDerecha().getResistenciaA(), n.getDerecha().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getIzquierda().setResistencia_actual(n.getIzquierda().actualizador_resistencia_a_velocidad_constante(n.getIzquierda().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), n.getIzquierda().getResistencia_actual(), n.getIzquierda().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getIzquierda().setAerodinamica_actual(n.getIzquierda().actualizador_aerodinamica_a_vel_constante(n.getIzquierda().getAerodinamica_actual(), n.getIzquierda().getAerodinamica(), n.getOdometro().getVelocidad_actual(), n.getIzquierda().getResistenciaA(), n.getIzquierda().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getEstanque().setCantidad_actual(n.getEstanque().actualizador_combustible_a_velocidad_constante(n.getEstanque().getCombustible().getQuemaje(), n.getEstanque().getCantidad_actual(), n.getOdometro().getVelocidad_actual()));
					
					n.getOdometro().setKm_actual(n.getOdometro().actualizador_km_a_velocidad_constante(n.getOdometro().getKm_actual(), n.getOdometro().getVelocidad_actual()));
					
					if (n.getMotor().getResistencia_actual() <= 0f) {
						n.getMotor().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getResistencia_actual() <= 0f) {
						n.getDerecha().setResistencia_actual(0f);
						n.getIzquierda().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getAerodinamica_actual() <= 0f) {
						n.getDerecha().setAerodinamica_actual(0f);
						n.getDerecha().setAerodinamica_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getEstanque().getCantidad_actual() <= 0f) {
						n.getEstanque().setCantidad_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}					
				}
				
				
				if (n.getEstado() == "a por reparacion") {
					float capacidades_actuales = ((n.getDerecha().getResistencia_actual() * 100f) + (n.getMotor().getResistencia_actual() * 100f));
					float capacidades_maximas = (n.getDerecha().getResistenciaA() + n.getMotor().getResistenciaM()), porcentaje_actual_de_capacidades = (capacidades_actuales / capacidades_maximas);
					float capacidad_de_velocidad = (porcentaje_actual_de_capacidades * n.getOdometro().getVelocidad_actual()), velocidad_nueva = (capacidad_de_velocidad / 100f);
					
					n.getMotor().setResistencia_actual(n.getMotor().actualizador_resistencia_acelerando(n.getOdometro().getVelocidad_actual(), velocidad_nueva, n.getMotor().getResistencia_actual(), n.getMotor().getResistenciaM(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getDerecha().setResistencia_actual(n.getDerecha().actualizador_resistencia_acelerando(n.getDerecha().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), velocidad_nueva, n.getDerecha().getResistencia_actual(), n.getDerecha().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getDerecha().setAerodinamica_actual(n.getDerecha().actualizador_aerodinamica_acelerando(n.getDerecha().getAerodinamica_actual(), n.getDerecha().getAerodinamica(), n.getOdometro().getVelocidad_actual(), velocidad_nueva, n.getDerecha().getResistenciaA(), n.getDerecha().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getIzquierda().setResistencia_actual(n.getIzquierda().actualizador_resistencia_acelerando(n.getIzquierda().getAerodinamica_actual(), n.getOdometro().getVelocidad_actual(), velocidad_nueva, n.getIzquierda().getResistencia_actual(), n.getIzquierda().getResistenciaA(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					n.getIzquierda().setAerodinamica_actual(n.getIzquierda().actualizador_aerodinamica_acelerando(n.getIzquierda().getAerodinamica_actual(), n.getIzquierda().getAerodinamica(), n.getOdometro().getVelocidad_actual(), velocidad_nueva, n.getIzquierda().getResistenciaA(), n.getIzquierda().getResistencia_actual(), n.getMotor().getCapacidad_maxima_de_velocidad_del_motor()));
					
					n.getEstanque().setCantidad_actual(n.getEstanque().actualizador_combustible_acelerando(n.getEstanque().getCombustible().getQuemaje(), n.getEstanque().getCantidad_actual(), n.getOdometro().getVelocidad_actual(), velocidad_nueva));
					
					n.getOdometro().setKm_actual(n.getOdometro().actualizador_km_acelerando(n.getOdometro().getKm_actual(), n.getOdometro().getVelocidad_actual(), velocidad_nueva));
					n.getOdometro().setVelocidad_actual(n.getOdometro().acelerador(velocidad_nueva, n.getOdometro().getVelocidad_actual()));
					
					
					if (n.getMotor().getResistencia_actual() <= 0f) {
						n.getMotor().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getResistencia_actual() <= 0f) {
						n.getDerecha().setResistencia_actual(0f);
						n.getIzquierda().setResistencia_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getDerecha().getAerodinamica_actual() <= 0f) {
						n.getDerecha().setAerodinamica_actual(0f);
						n.getDerecha().setAerodinamica_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
					
					if (n.getEstanque().getCantidad_actual() <= 0f) {
						n.getEstanque().setCantidad_actual(0f);
						n.getOdometro().setVelocidad_actual(0f);
						n.setAndando(false);
					}
				}
				
				
				if (n.getEstado() == "reparacion") {
					for (Reparador r: pista.getReparadores()) {
						n.getMotor().setResistencia_actual(r.reparacion_motor(n.getMotor().getResistencia_actual(), n.getMotor().getResistenciaM()));
						
						n.getDerecha().setResistencia_actual(r.reparacion_alas(n.getDerecha().getResistencia_actual()));
						n.getDerecha().setAerodinamica_actual(n.getDerecha().aerodinamica_recuperada(n.getDerecha().getResistenciaA(), n.getDerecha().getResistencia_actual(), n.getDerecha().getAerodinamica()));
						
						n.getIzquierda().setResistencia_actual(r.reparacion_alas(n.getIzquierda().getResistencia_actual()));
						n.getIzquierda().setAerodinamica_actual(n.getIzquierda().aerodinamica_recuperada(n.getIzquierda().getResistenciaA(), n.getIzquierda().getResistencia_actual(), n.getIzquierda().getAerodinamica()));
						
						float ochenta_porcientoM = ((n.getMotor().getResistenciaM() / 5f) * 4f), ochenta_porcientoA = ((n.getDerecha().getResistenciaA() / 5f) * 4f);						
						if ((n.getMotor().getResistencia_actual() >= ochenta_porcientoM) && (n.getDerecha().getResistencia_actual() >= ochenta_porcientoA)) {
							n.setAndando(true);							
						}						
					}
				}				
			}
			
			for (Nave n: pista.getNaves()) {
				
				
				int distancia = Math.round((n.getOdometro().getKm_actual() / 100));
				String nombre_nave = n.getNombre();
				Locutor.imprimir(distancia);
				System.out.println(nombre_nave);
				
				if (n.getOdometro().getKm_actual() >= 6000f) {
					finalistas = finalistas + 1;
				}
				
				Collections.sort(pista.getNaves(), new Comparator<Nave>()
				{
					
					public int compare (Nave n1, Nave n2) {
						return Float.valueOf(n2.getOdometro().getKm_actual()).compareTo(n1.getOdometro().getKm_actual());
					}
					
				});
				
			}
			
			tiempo_en_carrera = tiempo_en_carrera + 15;
			
			
			
			if (tiempo_en_carrera == 7200 || finalistas >= 1) {
				
				System.out.print("Los competidores terminaron la carrera en el sigte. orden: ");
				for (Nave n: pista.getNaves()) {
					System.out.println(n.getNombre());
				}
				
				playing = false;
			}			
	   }		
	}


	public Pista getPista() {
		return pista;
	}


	public void setPista(Pista pista) {
		this.pista = pista;
	}
}