import java.util.Random;
public class Nave_biodiesel implements Nave {
	
	private String nombre;
	
	private String estado;
	int delanteros = 0;
	
	private Boolean andando = true;
	private Ala derecha;
	private Ala izquierda;
	private Motor motor;
	private Estanque estanque;
	private Odometro odometro;
	private int number;
	//private int number_variacion_velocidad;
	
	Random numero = new Random();	
	
	
	Nave_biodiesel(String nombre){
		
		setNombre(nombre);
		this.estanque = new Estanque();
		this.motor = new Motor();
		this.odometro = new Odometro();
		this.derecha = new Ala();
		this.izquierda = new Ala();
		this.estanque.definir_combustible();
		definir_motor();
		definir_alas();
		definir_odometro();
	}
	
	
	public Boolean getAndando() {
		return andando;
	}
	public void setAndando(Boolean andando) {
		this.andando = andando;
	}
	
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
	
	public int getDelanteros() {
		return delanteros;
	}
	public void setDelanteros(int delanteros) {
		this.delanteros = delanteros;
	}


	public Ala getDerecha() {
		return derecha;
	}
	public void setDerecha(Ala derecha) {
		this.derecha = derecha;
	}
	
	public Ala getIzquierda() {
		return izquierda;
	}
	public void setIzquierda(Ala izquierda) {
		this.izquierda = izquierda;
	}
	
	
	public Motor getMotor() {
		return motor;
	}
	public void setMotor(Motor motor) {
		this.motor = motor;
	}


	public Estanque getEstanque() {
		return estanque;
	}
	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}


	public Odometro getOdometro() {
		return odometro;
	}
	public void setOdometro(Odometro odometro) {
		this.odometro = odometro;
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	public void definir_motor() {
		if (estanque.getCombustible().getNombre() == "Nuclear") {
			motor.setResistenciaM(3000.0f);
			motor.setResistencia_actual(3000.0f);
		}
		else {
			motor.setResistenciaM(2000.0f);
			motor.setResistencia_actual(2000.0f);
			motor.setCapacidad_de_aceleracion(1100f);
			motor.setCapacidad_maxima_de_velocidad_del_motor(3200f);
		}
	}
	public void definir_alas(){
		
		Random numero = new Random();		
		number = numero.nextInt(3);
		
		
		if (number == 0) {
			derecha.setAerodinamica(1800.0f);
			derecha.setAerodinamica_actual(1800.0f);
			derecha.setResistenciaA(2000.0f);
			derecha.setResistencia_actual(2000.0f);
			
			izquierda.setAerodinamica(1800.0f);
			izquierda.setAerodinamica_actual(1800.0f);
			izquierda.setResistenciaA(2000.0f);
			izquierda.setResistencia_actual(2000.0f);
		}
		if (number == 1) {
			derecha.setAerodinamica(2000.0f);
			derecha.setAerodinamica_actual(2000.0f);
			derecha.setResistenciaA(2000.0f);
			derecha.setResistencia_actual(2000.0f);
			
			izquierda.setAerodinamica(2000.0f);
			izquierda.setAerodinamica_actual(2000.0f);
			izquierda.setResistenciaA(2000.0f);
			izquierda.setResistencia_actual(2000.0f);
		}
		if (number == 2) {
			derecha.setAerodinamica(2300.0f);
			derecha.setAerodinamica_actual(2300.0f);
			derecha.setResistenciaA(2000.0f);
			derecha.setResistencia_actual(2000.0f);
			
			izquierda.setAerodinamica(2300.0f);
			izquierda.setAerodinamica_actual(2300.0f);
			izquierda.setResistenciaA(2000.0f);
			izquierda.setResistencia_actual(2000.0f);
		}
		
	}
	
	public void definir_odometro() {
		odometro.setKm_actual(0);
	}
	
	public void definir_estanque() {
		estanque.setCantidad_actual(estanque.getCapacidad());
		estanque.setNumber(2);
	}

}
