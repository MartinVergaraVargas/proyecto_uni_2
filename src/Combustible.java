
public class Combustible {
	
	private String nombre;
	private int quemaje;
	private float indice_aceleracion;
	
	
	Combustible(){		
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getQuemaje() {
		return quemaje;
	}
	public void setQuemaje(int quemaje) {
		this.quemaje = quemaje;
	}


	public float getIndice_aceleracion() {
		return indice_aceleracion;
	}
	public void setIndice_aceleracion(float indice_aceleracion) {
		this.indice_aceleracion = indice_aceleracion;
	}
}
